/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.munaw.micropay.admin.web.rest.dto;
