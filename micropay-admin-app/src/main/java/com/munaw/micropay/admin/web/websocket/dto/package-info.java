/**
 * Data Access Objects used by WebSocket services.
 */
package com.munaw.micropay.admin.web.websocket.dto;
