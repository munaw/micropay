package com.munaw.micropay.account;

import com.munaw.micropay.shared.dto.Account;
import com.munaw.micropay.shared.dto.Money;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

/**
 * @author munaw
 */
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProcessTransferRequest {
    protected String requestId;
    protected Account sender;
    protected Account recipient;
    protected Money money;

}
