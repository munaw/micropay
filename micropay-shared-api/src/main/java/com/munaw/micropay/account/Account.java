package com.munaw.micropay.account;

/**
 * @author munaw
 */
public interface Account {
    String getId();

    void setId(String id);

    String getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);
}
