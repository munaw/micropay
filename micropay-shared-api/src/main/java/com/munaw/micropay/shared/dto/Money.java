package com.munaw.micropay.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author munaw
 */
@JsonDeserialize
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Money {
	BigDecimal amount;
	Currency currency;

    public static Money of(float amount, String currencyCode) {
        return new Money(new BigDecimal(amount), Currency.getInstance(currencyCode));
    }
}
