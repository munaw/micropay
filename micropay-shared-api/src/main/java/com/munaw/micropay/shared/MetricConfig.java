package com.munaw.micropay.shared;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

/**
 * @author munaw
 */
@Configuration
@EnableMetrics
public class MetricConfig extends MetricsConfigurerAdapter {

	@Value("${spring.application.name}")
	protected String appName="defaultAppName";
	@Override
	public void configureReporters(MetricRegistry metricRegistry) {
		final Graphite graphite = new Graphite(new InetSocketAddress("ubuntu3", 2003));
		final GraphiteReporter reporter = GraphiteReporter.forRegistry(metricRegistry)
				.prefixedWith(appName)
				.convertRatesTo(TimeUnit.SECONDS)
				.convertDurationsTo(TimeUnit.MILLISECONDS)
				.filter(MetricFilter.ALL)
				.build(graphite);
//		reporter.start(1, TimeUnit.MINUTES);
//		ConsoleReporter
//				.forRegistry(metricRegistry)
//				.build()
//				.start(1, TimeUnit.MINUTES);
	}

}