package com.munaw.micropay.shared.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class TransactionId {
    String id;

    public TransactionId(String id) {
        this.id = id;
    }
}
