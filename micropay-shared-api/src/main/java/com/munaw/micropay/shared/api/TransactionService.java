package com.munaw.micropay.shared.api;

import com.munaw.micropay.shared.dto.TransactionId;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.shared.dto.TransferConfirmationResult;
import com.munaw.micropay.shared.dto.TransferRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author munaw
 */

//@FeignClient("micropay-transaction-service")
public interface TransactionService {
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    TransactionId initTransfer(TransferRequest transferRequest);

    @RequestMapping(value = "/transaction/{id}/metadata/{key}", method = RequestMethod.PUT)
    void setTransactionMetadata(
        @PathVariable("id") TransactionId id,
        @PathVariable("key") String key,
        @RequestBody String value
    );

    @RequestMapping(value = "/transaction/{id}/metadata/{key}", method = RequestMethod.GET)
    String getTransactionMetadata(
        @PathVariable("id") TransactionId id,
        @PathVariable("key") String key
    );


    @RequestMapping(value = "/transaction/{id}/confirm", method = RequestMethod.POST)
    TransferConfirmationResult confirmTransfer(@PathVariable("id") TransactionId id, TransferConfirmation confirmation);

}
