package com.munaw.micropay.shared.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

/**
 * @author munaw
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransferConfirmationResult {
    private boolean approved = false;

    public boolean isApproved() {
        return approved;
    }
}
