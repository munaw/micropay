package com.munaw.micropay.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author munaw
 */
@JsonDeserialize
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferRequest implements Serializable {
    protected String requestId;
    protected ParticipantDetails sender;
    protected ParticipantDetails recipient;
    protected Money money;
    Map<String, String> metadata = new HashMap<String, String>();

    public void addMetadata(final String key, final String val) {
        if (metadata == null) metadata = new HashMap<String, String>();
        metadata.put(key, val);
    }
}
