package com.munaw.micropay.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author munaw
 */
@JsonDeserialize
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferRequestId implements Serializable{
	private UUID id;
}
