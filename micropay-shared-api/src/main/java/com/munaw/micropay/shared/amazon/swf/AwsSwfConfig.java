package com.munaw.micropay.shared.amazon.swf;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflowClient;
import com.amazonaws.services.simpleworkflow.flow.spring.WorkflowScope;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class AwsSwfConfig {

    @Bean
    public CustomScopeConfigurer customScopeConfigurer() {
        final CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
        customScopeConfigurer.addScope("workflow", new WorkflowScope());
        return customScopeConfigurer;
    }


    @Bean
    @Primary
    public AmazonSimpleWorkflow getSimpleWorkflowClient(
            AWSCredentialsProvider awsCredentialsProvider,
            ClientConfiguration config

    ) {
        AmazonSimpleWorkflowClient amazonSimpleWorkflowClient = new AmazonSimpleWorkflowClient(awsCredentialsProvider, config);
//        amazonSimpleWorkflowClient.setRegion(Region.getRegion(Regions.US_EAST_1));
        return amazonSimpleWorkflowClient;
    }

    @Bean
    public ClientConfiguration getConnectionConfiguration() {
        return new ClientConfiguration().withSocketTimeout(70 * 1000);
    }

}
