package com.munaw.micropay.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

/**
 * @author munaw
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParticipantDetails {
	public String externalId;
	public String internalId;
	public String emailAddress;
	public String cellphoneNumber;
	public String otpKey;
    public Account account;

	public static ParticipantDetails withEmail(String emailAddress){
		final ParticipantDetails participantDetail = new ParticipantDetails();
		participantDetail.emailAddress = emailAddress;
		return participantDetail;
	}

}
