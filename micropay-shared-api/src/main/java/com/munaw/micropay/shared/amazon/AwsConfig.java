package com.munaw.micropay.shared.amazon;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfig {

    @Bean
    public AWSCredentialsProvider awsCredentialsProvider() throws Exception {
        return new DefaultAWSCredentialsProviderChain();
    }


    @Bean
    public ClientConfiguration getConnectionConfiguration() {
        return new ClientConfiguration().withSocketTimeout(70 * 1000);
    }
}
