package com.munaw.micropay.shared.api;

import com.munaw.micropay.account.Account;
import com.munaw.micropay.account.ProcessTransferRequest;
import com.munaw.micropay.account.TransferResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author munaw
 */
//@FeignClient("micropay-account-service")
@RequestMapping("/accounts")
public interface AccountService {
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Account findOne(@PathVariable("id") String id);

    @RequestMapping(value = "/transfer", method = RequestMethod.PUT)
    TransferResult processTransfer(ProcessTransferRequest processTransferRequest);
}
