package com.munaw.micropay.shared.dto;

import com.munaw.micropay.user.UserIdentity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Builder;
import org.hibernate.validator.constraints.Email;

/**
 * @author munaw
 */
@Data
@Getter
@Setter
@Builder
public class User implements UserIdentity {
    protected Long id;
    protected String accountId;
    @Email
    protected String email;
    protected AuthType authType;
    protected String userId;
    protected boolean draft;

    public static User of(final UserIdentity userIdentity) {
        return User.builder()
            .id(userIdentity.getId())
            .accountId(userIdentity.getAccountId())
            .email(userIdentity.getEmail())
            .authType(userIdentity.getAuthType())
            .userId(userIdentity.getUserId())
            .build();
    }
}
