package com.munaw.micropay.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.hibernate.validator.constraints.Email;

/**
 * @author munaw
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account implements com.munaw.micropay.account.Account {
    protected String id;
    @Email
	protected String firstName;
	protected String lastName;

    static public Account of(com.munaw.micropay.account.Account account) {
        return Account.builder()
            .firstName(account.getFirstName())
            .lastName(account.getLastName())
            .id(account.getId())
            .build();
    }
}
