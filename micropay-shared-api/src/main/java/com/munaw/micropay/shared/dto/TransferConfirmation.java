package com.munaw.micropay.shared.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

/**
 * @author munaw
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransferConfirmation {
    TransactionId transactionId;
    String confirmationCode;
    String otp;
}
