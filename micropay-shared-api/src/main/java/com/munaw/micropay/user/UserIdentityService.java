package com.munaw.micropay.user;

import com.munaw.micropay.shared.dto.User;

import java.util.Optional;

/**
 * @author munaw
 */
public interface UserIdentityService {
    Optional<User> findByEmail(String email);

    void save(UserIdentity userIdentity);
}
