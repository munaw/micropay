package com.munaw.micropay.user;

/**
 * @author munaw
 */
public interface UserIdentity {
    Long getId();

    void setId(Long id);

    String getAccountId();

    void setAccountId(String accountId);

    AuthType getAuthType();

    void setAuthType(AuthType authType);

    String getUserId();

    void setUserId(String userId);

    String getEmail();

    void setEmail(String email);

    boolean equals(Object o);

    int hashCode();

    String toString();

    boolean isDraft();

    void setDraft(boolean draft);

    public enum AuthType {
        USERPASS, CELLPHONE, EMAIL, OAUTH2TOKEN
    }
}
