package com.munaw.micropay.account;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * @author munaw
 */
@RepositoryRestResource
public interface AccountRepository extends CrudRepository<AccountEntity, String> {
}
