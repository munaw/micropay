package com.munaw.micropay.account;

import com.munaw.micropay.shared.api.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import javax.inject.Inject;

/**
 * @author munaw
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Inject
    AccountRepository accountRepository;

    @Override
    public Account findOne(@PathVariable("id") final String id) {
        return accountRepository.findOne(id);
    }

    @Override
    public TransferResult processTransfer(final ProcessTransferRequest processTransferRequest) {
        return new TransferResult();
    }


}
