package com.munaw.micropay.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableEurekaClient
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class AccountServiceApplication {
	@Autowired
	private AccountRepository accountRepository;


	public static void main(final String[] args) {
		SpringApplication.run(AccountServiceApplication.class, args);
	}

	@PostConstruct
	public void postInit(){
		try {
//            AccountEntity account1 = new AccountEntity(1234L, "Mano", "Kovacs");
//            AccountEntity account2 = new AccountEntity(5678L, "John", "Smith");
//			accountRepository.save(account1);
//			accountRepository.save(account2);
        } catch (Exception e) {
//			e.printStackTrace();
		}
	}


}
