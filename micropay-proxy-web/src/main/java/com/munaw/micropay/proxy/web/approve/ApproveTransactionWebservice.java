package com.munaw.micropay.proxy.web.approve;

import com.munaw.micropay.shared.dto.TransferConfirmation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author munaw
 */

@RestController("/approve")
@Slf4j
public class ApproveTransactionWebservice {
	@Value("${sqs.transaction-manager-approveTransfer-topic}")
	private String queueName;

	@Autowired
	private QueueMessagingTemplate queueMessagingTemplate;

	@RequestMapping(method = RequestMethod.POST)
	public void approveTransaction(ApproveTransactionRequest approveRequest) {
		log.info("Transaction approval webrequest " + approveRequest.toString());
		queueMessagingTemplate.convertAndSend(queueName, createTransactionApproval(approveRequest));
	}

    private TransferConfirmation createTransactionApproval(final ApproveTransactionRequest approveRequest) {
        return new TransferConfirmation(approveRequest.getTransactionId(), approveRequest.getConfirmationCode());
    }
}
