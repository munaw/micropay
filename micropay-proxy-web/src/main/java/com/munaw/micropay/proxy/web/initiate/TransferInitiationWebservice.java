package com.munaw.micropay.proxy.web.initiate;

import com.munaw.micropay.shared.api.TransactionService;
import com.munaw.micropay.shared.dto.TransferRequest;
import com.munaw.micropay.shared.dto.TransferRequestId;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author munaw
 */

@RestController("/transfer")
@Slf4j
public class TransferInitiationWebservice {
	@Autowired
    protected TransactionService initiationService;

	@RequestMapping(method = RequestMethod.POST)
	public String initiatePayment(TransferInitiationRequest initRequest){
		log.info("Keres");
		TransferRequestId transferRequestId = initiateTransfer();
//		log.info(transferRequestId.toString());
		return transferRequestId.getId();
	}

	@HystrixCommand(commandProperties = {
			@HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="1000"),
			@HystrixProperty(name="circuitBreaker.requestVolumeThreshold", value="1")
	}, fallbackMethod = "openCircuit")
	private TransferRequestId initiateTransfer() {
		log.info("herewego");
        return initiationService.initTransfer(new TransferRequest());
    }

    public TransferRequestId openCircuit(){
		log.info("fck");
		return new TransferRequestId("fuck");
	}
}
