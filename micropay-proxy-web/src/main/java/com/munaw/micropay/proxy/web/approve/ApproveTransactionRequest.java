package com.munaw.micropay.proxy.web.approve;

import lombok.Getter;
import lombok.ToString;

/**
 * @author munaw
 */
@Getter
@ToString
public class ApproveTransactionRequest {
	String transactionId;
	String confirmationCode;
}
