package com.munaw.micropay.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author munaw
 */
@Repository
interface UserIdentityRepository extends CrudRepository<UserIdentityEntity, Long> {
    UserIdentityEntity findByEmail(String email);


}
