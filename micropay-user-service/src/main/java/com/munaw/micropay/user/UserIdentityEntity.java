package com.munaw.micropay.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import javax.persistence.*;

/**
 * @author munaw
 */
@Data
@Entity
@Table(name = "user_identity", uniqueConstraints = @UniqueConstraint(name = "uidx_email", columnNames = "email"))
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserIdentityEntity implements UserIdentity {
    @Id
    @GeneratedValue
    protected Long id;
    protected String accountId;
    protected AuthType authType;
    protected String userId;
    protected String secret;
    protected String email;
    protected boolean draft = false;

    public static UserIdentityEntity of(final UserIdentity userIdentity) {
        return UserIdentityEntity.builder()
            .id(userIdentity.getId())
            .accountId(userIdentity.getAccountId())
            .authType(userIdentity.getAuthType())
            .userId(userIdentity.getUserId())
            .email(userIdentity.getEmail())
            .draft(userIdentity.isDraft())
            .build();
    }
}
