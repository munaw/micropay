package com.munaw.micropay.user;

import com.munaw.micropay.shared.dto.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.Optional;

/**
 * @author munaw
 */
@Service
public class UserIdentityServiceImpl implements UserIdentityService {
    @Inject
    private UserIdentityRepository userIdentityRepository;

    @Override
    @Transactional
    public Optional<User> findByEmail(final String email) {
        UserIdentityEntity userByEmail = userIdentityRepository.findByEmail(email);
        if (userByEmail == null) {
            return Optional.empty();
        }
        return Optional.of(User.of(userByEmail));
    }

    @Override
    public void save(final UserIdentity userIdentity) {
        Assert.notNull(userIdentity);
        userIdentityRepository.save(UserIdentityEntity.of(userIdentity));
    }


}
