package com.munaw.micropay.transactionmanager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.munaw.micropay.shared.entity.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jadira.usertype.moneyandcurrency.joda.PersistentMoneyAmountAndCurrency;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author munaw
 */

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(name = "MoneyAmountWithCurrencyType", typeClass = PersistentMoneyAmountAndCurrency.class)
//@Table(uniqueConstraints = @UniqueConstraint(name = "uidx_request_id", columnNames = "request_id"))
public class Transaction extends AbstractEntity<String> {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid",
        strategy = "uuid")
    protected String id;
    @JsonIgnore
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "meta_key")
    @Column(name = "meta_value")
    @CollectionTable(
        name = "transaction_metadata",
        joinColumns = @JoinColumn(name = "transaction_id")
    )
    Map<String, String> metadata = new HashMap<String, String>();
    private String requestId;
    private States state = States.INITED;
    private String confirmationCode;
    private String senderUserId;
    private String senderAccountId;
    private String recipientUserId;
    private String recipientAccountId;
    @Type(type = "MoneyAmountWithCurrencyType")
    @Columns(columns = {@Column(name = "value_currency"), @Column(name = "value_amount")})
    private Money value;

	public boolean hasState(States state){
		return state == this.state;
	}

    public void setSenderAccountId(final UUID id) {
        this.senderAccountId = id.toString();
    }

    public void setSenderAccountId(final String id) {
        this.senderAccountId = id;
    }

    public void setSenderAccountId(final Long id) {
        this.senderAccountId = String.valueOf(id);
    }

    public void setValue(final com.munaw.micropay.shared.dto.Money money) {
        value = Money.of(CurrencyUnit.of(money.getCurrency()), money.getAmount());
    }

    public static enum States {
        INITED, INIT_APPROVED, PENDING;
    }
}
