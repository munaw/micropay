package com.munaw.micropay.transactionmanager;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author munaw
 */
@Repository
public interface TransactionRepository extends CrudRepository<Transaction, String> {
	Transaction findByRequestId(String requestId);


    Transaction findTop1ByOrderByCreatedAtDesc();
}
