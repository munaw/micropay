package com.munaw.micropay.transactionmanager;

import com.munaw.micropay.shared.api.TransactionService;
import com.munaw.micropay.shared.dto.TransactionId;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.shared.dto.TransferConfirmationResult;
import com.munaw.micropay.shared.dto.TransferRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author munaw
 */
@Slf4j
@RestController
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    protected TransactionRepository transactionRepository;


    @Override
    @Transactional
    public TransactionId initTransfer(final TransferRequest transferRequest) {
        String requestId = transferRequest.getRequestId();

        Transaction transaction = transactionRepository.findByRequestId(requestId);
        if (transaction == null) {
            transaction = new Transaction();
            transaction.setMetadata(transferRequest.getMetadata());
            transaction.setValue(transferRequest.getMoney());
            transaction.setRequestId(requestId);
            transaction.setSenderAccountId(transferRequest.getSender().getAccount().getId());
            transaction.setConfirmationCode(UUID.randomUUID().toString());
            transactionRepository.save(transaction);
        }
        return new TransactionId(transaction.getId());
    }

    @Override
    @Transactional
    public void setTransactionMetadata(@PathVariable("id") final TransactionId id, @PathVariable("key") final String key, @RequestBody final String value) {
        Transaction transaction = transactionRepository.findOne(id.getId().toString());
        if (transaction == null) {
            throw new ResourceNotFoundException();
        }
        transaction.getMetadata().put(key, value);
        transactionRepository.save(transaction);

    }

    @Override
    @Transactional
    public String getTransactionMetadata(@PathVariable("id") final TransactionId id, @PathVariable("key") final String key) {
        Transaction transaction = transactionRepository.findOne(id.getId().toString());
        if (transaction == null) {
            throw new ResourceNotFoundException();
        }
        return transaction.getMetadata().get(key);
    }

    @Override
    public TransferConfirmationResult confirmTransfer(@PathVariable("id") final TransactionId id, final TransferConfirmation confirmation) {
        Assert.assertEquals(id.getId(), confirmation.getTransactionId().getId());

        Transaction transaction = transactionRepository.findOne(id.getId().toString());
        if (transaction == null) {
            throw new ResourceNotFoundException();
        }

        return TransferConfirmationResult.builder()
            .approved(transaction.getConfirmationCode().equals(confirmation.getConfirmationCode()))
            .build();
    }
}
