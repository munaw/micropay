package com.munaw.micropay.transactionmanager;

import org.springframework.boot.SpringApplication;

/**
 * @author munaw
 */
//@SpringBootApplication
//@EnableEurekaClient
//@EnableFeignClients
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//@ComponentScan({"com.munaw.micropay.shared", "com.munaw.micropay.transactionmanager"})
public class TransactionManagerApplication {
	public static void main(final String[] args) {
		SpringApplication.run(TransactionManagerApplication.class, args);
	}

}
