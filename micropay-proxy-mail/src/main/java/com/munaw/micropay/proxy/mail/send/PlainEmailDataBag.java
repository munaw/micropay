package com.munaw.micropay.proxy.mail.send;

import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

import java.util.Map;

@Builder
@Data
@AllArgsConstructor
public class PlainEmailDataBag implements EmailDataBag {
    private String body;

    @Override
    public Map<String, Object> toData() {
        return ImmutableMap.of("body", body);
    }
}
