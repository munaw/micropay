package com.munaw.micropay.proxy.mail.template;


import freemarker.template.Configuration;

import java.io.File;
import java.io.IOException;

@org.springframework.context.annotation.Configuration
public class FreemarkerConfig {
    public Configuration configuration() throws IOException {
        Configuration configuration = new Configuration();
        configuration.setDirectoryForTemplateLoading(new File("classpath:/templates/"));
        return configuration;
    }
}
