package com.munaw.micropay.proxy.mail;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author munaw
 */

@Getter
@Slf4j
public class SendCommandEmail {


    private final String messageId;
    private String senderEmail;
    private String subject;
    private List<String> recipients;

    public SendCommandEmail(String messageId, String message) {
        this.messageId = messageId;
        JsonNode messageObj;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            messageObj = objectMapper.readTree(message);
            JsonNode mail = messageObj.get("mail");
            senderEmail = mail.get("commonHeaders").get("from").get(0).textValue();
//			senderEmail = mail.get("source").textValue();
            JsonNode destination = mail.get("destination");
            recipients = new ArrayList<String>();
            for (JsonNode email : destination) {
                recipients.add(email.textValue());
            }
            log.info(recipients.toString());
            subject = mail.get("commonHeaders").get("subject").textValue();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "EmailReceivedEvent{" +
            "senderEmail='" + senderEmail + '\'' +
            ", subject='" + subject + '\'' +
            ", recipients=" + recipients +
            '}';
    }
}
