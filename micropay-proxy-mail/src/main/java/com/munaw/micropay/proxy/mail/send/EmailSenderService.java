package com.munaw.micropay.proxy.mail.send;

import com.munaw.micropay.proxy.mail.template.TemplateService;
import freemarker.template.TemplateException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;


@Service
public class EmailSenderService {
    @Inject
    private JavaMailSender mailSender;
    @Inject
    private TemplateService templateService;

    public void send(EmailDescriptor email) {
        try {
            String emailBody;
            if (email.getTemplate() != null) {
                String tpl = String.format("email/%s", email.getTemplate());
                emailBody = templateService.process(tpl, email.getData());
            } else {
                emailBody = email.getText();
            }
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
            helper.setFrom(email.getSenderEmail());
            helper.setTo(email.getRecipientEmail());
            helper.setSubject(email.getSubject());
            for (String cc : email.getCcEmails()) {
                helper.addTo(cc);
            }
            ;
            helper.setText(emailBody, true);
            mailSender.send(message);
        } catch (TemplateException | IOException | MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
