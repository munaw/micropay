package com.munaw.micropay.proxy.mail.template;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class TemplateService {
    private final Configuration configuration;

    public String process(String tpl) throws TemplateException, IOException {
        return process(tpl, new HashMap<>());
    }

    public String process(String tpl, Map<String, Object> data) throws TemplateException, IOException {
        Template template = null;
        template = configuration.getTemplate(tpl);
        // Console output
        Writer out = new StringWriter();
        template.process(data, out);
        return out.toString();
    }
}
