package com.munaw.micropay.proxy.mail.send;

import java.util.Map;

/**
 * @author munaw
 */
public interface EmailDataBag {
    public Map<String, Object> toData();
}
