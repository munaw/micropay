package com.munaw.micropay.proxy.mail.send;

import lombok.Data;
import lombok.experimental.Builder;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author munaw
 */
@Data
@Builder
public class EmailDescriptor {
    private String subject;
    private String text = "";
    private String template = "plain";
    private String senderEmail;
    private String recipientEmail;
    private Set<String> ccEmails = new HashSet<>();
    private EmailDataBag dataBag;

    public Map<String, Object> getData() {
        return getDataBag().toData();
    }
}
