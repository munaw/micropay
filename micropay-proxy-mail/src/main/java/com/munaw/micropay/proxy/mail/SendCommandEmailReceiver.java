package com.munaw.micropay.proxy.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author munaw
 */
@Slf4j
@Component
public class SendCommandEmailReceiver {

	private static final String RECEIVER_QUEUE_NAME = "${sqs.send-commmand-topic}";

	@Autowired
	private TransactionInitiator transactionInitiator;

    public SendCommandEmailReceiver() {
        log.info("SendCommandEmailReceiver CONSTRUCTOR");
    }

    @MessageMapping(RECEIVER_QUEUE_NAME)
    void receive(@Header("MessageId") String messageId, @Header("SenderId") String senderId, String message) throws IOException {
		log.warn(messageId);
		log.warn(senderId);
		log.info(message);
		SendCommandEmail sendCommandEmail = new SendCommandEmail(messageId, message);
		log.info(sendCommandEmail.toString());
		transactionInitiator.initiate(sendCommandEmail);
	}
}
