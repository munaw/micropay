package com.munaw.micropay.proxy.mail;

import com.munaw.micropay.shared.dto.Money;
import com.munaw.micropay.shared.dto.ParticipantDetails;
import com.munaw.micropay.shared.dto.TransferRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author munaw
 */
@Component
public class TransactionInitiator {

    private static final String SENDEMAIL = "send@aws.munaw.com";
    @Value("${sqs.workflow-service-initTransfer-topic}")
    private String queueName;
    @Autowired
    private QueueMessagingTemplate queueMessagingTemplate;

    public void initiate(final SendCommandEmail sendCommandEmail) {
        ParticipantDetails sender = ParticipantDetails.builder()
            .emailAddress(sendCommandEmail.getSenderEmail())
            .build();
        ParticipantDetails recipient = ParticipantDetails.builder()
            .emailAddress(
                sendCommandEmail
                    .getRecipients()
                    .stream()
                    .filter(e -> !SENDEMAIL.contains(e))
                    .findFirst().orElse("")
            )
            .build();
        Money money = extractMoneyFromSubject(sendCommandEmail.getSubject());

        final TransferRequest initTransferRequest = TransferRequest.builder()
            .requestId(sendCommandEmail.getMessageId())
            .sender(sender)
            .recipient(recipient)
            .money(money)
            .build();

        queueMessagingTemplate.convertAndSend(queueName, initTransferRequest);
    }

    private Money extractMoneyFromSubject(final String subject) {
        BigDecimal amount = new BigDecimal(subject.replaceAll("[^\\d.]+", ""));
        String currSubject = subject.replace("$", "USD").replace("£", "GBP");
        Matcher matcher = Pattern.compile("[A-Z]{3}").matcher(currSubject);
        Currency curr = null;
        if (matcher.find()) curr = Currency.getInstance(matcher.group());
        return new Money(amount, curr);
    }
}
