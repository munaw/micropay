package com.munaw.micropay.proxy.mail;

import com.amazonaws.services.sqs.AmazonSQS;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.core.env.ResourceIdResolver;
import org.springframework.cloud.aws.messaging.config.SimpleMessageListenerContainerFactory;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author munaw
 */
//@EnableEurekaClient
@SpringBootApplication
@EnableAutoConfiguration
@EnableCircuitBreaker
@ComponentScan({"com.munaw.micropay.shared", "com.munaw.micropay.proxy.mail"})
//@EnableFeignClients(basePackages = {"com.munaw.micropay.shared.api"})
public class MailProxyApplication {
	public static void main(String[] args) {
		SpringApplication.run(MailProxyApplication.class, args);
	}

}
