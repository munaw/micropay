package com.munaw.micropay;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;
import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientExternal;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientExternalFactory;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientExternalFactoryImpl;
import com.munaw.micropay.workflow.workflow.TransferWorkflowImpl;
import com.munaw.micropay.workflow.workflow.activities.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * @author munaw
 */
@SpringBootApplication
//@EnableEurekaClient
@EnableFeignClients(basePackages = "com.munaw.micropay.shared.api")
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableJpaRepositories(basePackages = "com.munaw.micropay")
@ComponentScan({"com.munaw.micropay"})
public class WorkflowManagerApplication {
	public static void main(final String[] args) {
		SpringApplication.run(WorkflowManagerApplication.class, args);
	}

    @Bean
    @Inject
    public TransferWorkflowClientExternal transferWorkflowClientExternal(
        AmazonSimpleWorkflow swfClient,
        @Value("${aws.swf.domain}") String swfDomain
    ){
        return new TransferWorkflowClientExternalFactoryImpl(swfClient, swfDomain).getClient();
    }



    @Bean
    public TransferWorkflowClientExternalFactory transferWorkflowClientExternalFactory(
        AmazonSimpleWorkflow swfClient,
        @Value("${aws.swf.domain}") final String domain
    ) {
        return new TransferWorkflowClientExternalFactoryImpl(swfClient, domain);
    }

    @Bean
    @Scope("workflow")
    public TransferActivitiesClient orderLifecycleActivitiesClient() {
        return new TransferActivitiesClientImpl();
    }

    @Bean
    @Scope("workflow")
    public UserServiceActivitiesClient userServiceActivitiesClient() {
        return new UserServiceActivitiesClientImpl();
    }

    @Bean
    @Inject
    public ActivityWorker activityWorker(
        AmazonSimpleWorkflow swfClient,
        @Value("${aws.swf.domain}") String domain,
        @Value("${aws.swf.transfer.tasklist}") String tasklist,
        TransferActivities transferActivities
    ) throws IllegalAccessException, NoSuchMethodException, InstantiationException {
        ActivityWorker activityWorker = new ActivityWorker(swfClient, domain, tasklist);
        activityWorker.addActivitiesImplementation(transferActivities);
        activityWorker.start();
        return activityWorker;

    }

    @Bean
    @Inject
    public WorkflowWorker workflowWorker(
        AmazonSimpleWorkflow swfClient,
        @Value("${aws.swf.domain}") String domain,
        @Value("${aws.swf.transfer.tasklist}") String tasklist,
        TransferActivities transferActivities
    ) throws IllegalAccessException, InstantiationException {
        WorkflowWorker workflowWorker = new WorkflowWorker(swfClient, domain, tasklist);
        workflowWorker.addWorkflowImplementationType(TransferWorkflowImpl.class);
        workflowWorker.start();
        return workflowWorker;

    }

    @Service
    static public class Workers {
        @Inject
        public Workers(ActivityWorker activityWorker, WorkflowWorker workflowWorker) {
        }
    }

}
