package com.munaw.micropay.transactionmanager.remote.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author munaw
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
	protected String id;
	protected String email;
	protected String firstName;
	protected String lastName;
}
