package com.munaw.micropay.workflow.workflow.activities;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;
import com.amazonaws.services.simpleworkflow.flow.annotations.ExponentialRetry;
import com.munaw.micropay.account.Account;
import com.munaw.micropay.account.ProcessTransferRequest;
import com.munaw.micropay.shared.dto.*;
import com.munaw.micropay.user.UserIdentity;

@ActivityRegistrationOptions(defaultTaskScheduleToStartTimeoutSeconds = 1000,
    defaultTaskStartToCloseTimeoutSeconds = 100)
@Activities(version = "1.1")
public interface TransferActivities {
    @ExponentialRetry(initialRetryIntervalSeconds = 10, maximumAttempts = 10, backoffCoefficient = 2)
    Account findAccountByUserEmail(String email);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 5, backoffCoefficient = 1.8)
    TransactionId createTransaction(TransferRequest transferRequest);

    @ExponentialRetry(initialRetryIntervalSeconds = 30, maximumAttempts = 4, backoffCoefficient = 2)
    void sendConfirmationRequest(TransactionId id);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 10, backoffCoefficient = 1.1)
    TransferConfirmationResult confirmTransaction(TransferConfirmation transferConfirmation);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 10, backoffCoefficient = 1.5)
    User inviteUserForTransaction(TransactionId txnid, ParticipantDetails recipient);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 4, backoffCoefficient = 1.5)
    void sendRecipientNotification(UserIdentity userIdentity, TransferRequest transferRequest);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 10, backoffCoefficient = 1.5)
    void registerRecipient(User recipient);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 10, backoffCoefficient = 1.5)
    String createTransactionConfirmationId(TransactionId id);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 5, backoffCoefficient = 3)
    void transferAmount(TransactionId id, String confirmationId);

    @ExponentialRetry(initialRetryIntervalSeconds = 60, maximumAttempts = 5, backoffCoefficient = 3)
    void processTransfer(ProcessTransferRequest processTransferRequest);

}
