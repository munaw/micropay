package com.munaw.micropay.workflow;

import com.munaw.micropay.shared.dto.TransferRequest;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientExternalFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @author munaw
 */
@Slf4j
@Component
public class TransferRequestReceiver {

    private static final String QUEUE_NAME = "${sqs.workflow-service-initTransfer-topic}";

    @Inject
    private TransferWorkflowClientExternalFactory transferWorkflowClientExternalFactory;

    @MessageMapping(QUEUE_NAME)
    public void receive(TransferRequest transferRequest) {
        log.info(transferRequest.toString());
        transferWorkflowClientExternalFactory
            .getClient()
            .initiateTransaction(transferRequest);
    }
}
