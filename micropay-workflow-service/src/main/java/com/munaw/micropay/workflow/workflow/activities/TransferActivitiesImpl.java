package com.munaw.micropay.workflow.workflow.activities;

import com.amazonaws.services.simpleworkflow.flow.ActivityExecutionContextProvider;
import com.amazonaws.services.simpleworkflow.flow.ActivityExecutionContextProviderImpl;
import com.munaw.micropay.account.Account;
import com.munaw.micropay.account.ProcessTransferRequest;
import com.munaw.micropay.shared.api.AccountService;
import com.munaw.micropay.shared.api.TransactionService;
import com.munaw.micropay.shared.dto.*;
import com.munaw.micropay.user.UserIdentity;
import com.munaw.micropay.user.UserIdentityService;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Optional;

import static com.munaw.micropay.workflow.TransactionMetadata.WORKFLOW_ID;

/**
 * @author munaw
 */
@Component
@Log4j
public class TransferActivitiesImpl implements TransferActivities {
    ActivityExecutionContextProvider activityExecutionContextProvider = new ActivityExecutionContextProviderImpl();

    @Inject
    TransactionService transactionService;

    @Inject
    UserIdentityService userIdentityService;

    @Inject
    AccountService accountService;

    @Override
    public Account findAccountByUserEmail(final String email) {
        log.info("findAccountByUserEmail");
        Optional<User> userIdentity = userIdentityService.findByEmail(email);
        return userIdentity
            .map(user -> accountService.findOne(user.getAccountId()))
            .orElse(null);
    }

    @Override
    public TransactionId createTransaction(final TransferRequest transferRequest) {
        log.info("createTransaction");
        String wfid = activityExecutionContextProvider
            .getActivityExecutionContext()
            .getWorkflowExecution()
            .getWorkflowId();
        transferRequest.addMetadata(WORKFLOW_ID, wfid);
        return transactionService.initTransfer(transferRequest);
    }

    @Override
    public void sendConfirmationRequest(TransactionId id) {
        log.info("sendConfirmationRequest");
    }

    @Override
    public TransferConfirmationResult confirmTransaction(TransferConfirmation transferConfirmation) {
        log.info("confirmTransaction");
        return transactionService.confirmTransfer(transferConfirmation.getTransactionId(), transferConfirmation);
    }

    @Override
    public User inviteUserForTransaction(final TransactionId txnid, final ParticipantDetails recipient) {
        log.info("inviteUserForTransaction");
//        return User.of(userIdentityService.createDraftUserForTransaction(
//            User.builder().email(recipient.getEmailAddress()).build(), txnid
//        ));
        return null;
    }

    @Override
    public void sendRecipientNotification(UserIdentity userIdentity, TransferRequest transferRequest) {
        log.info("sendRecipientNotification");
        // @TODO
    }

    @Override
    public void registerRecipient(final User recipient) {
        log.info("registerRecipient");

    }

    @Override
    public String createTransactionConfirmationId(final TransactionId id) {
        log.info("createTransactionConfirmationId");
        return null;
    }

    @Override
    public void transferAmount(final TransactionId id, final String confirmationId) {
        log.info("transferAmount");

    }

    @Override
    public void processTransfer(final ProcessTransferRequest processTransferRequest) {
        log.info("processTransfer");
        accountService.processTransfer(processTransferRequest);
    }
}
