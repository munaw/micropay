package com.munaw.micropay.transactionmanager.remote.account;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author munaw
 */
@FeignClient("micropay-account-service")
@RequestMapping("/accounts")
public interface AccountService {
	@RequestMapping(value = "/search/findByEmail?email={email}", method = RequestMethod.GET)
	public Account findByEmail(@PathVariable("email") String email);
}
