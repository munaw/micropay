package com.munaw.micropay.workflow.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Execute;
import com.amazonaws.services.simpleworkflow.flow.annotations.Signal;
import com.amazonaws.services.simpleworkflow.flow.annotations.Workflow;
import com.amazonaws.services.simpleworkflow.flow.annotations.WorkflowRegistrationOptions;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.shared.dto.TransferRequest;

@Workflow
@WorkflowRegistrationOptions(defaultExecutionStartToCloseTimeoutSeconds = 15552000 /* 180 days */)
public interface TransferWorkflow {
    @Execute(version = "1.0")
    public void initiateTransaction(TransferRequest transferRequest);

    @Signal
    public void handleConfirmation(TransferConfirmation transferConfirmation);

}
