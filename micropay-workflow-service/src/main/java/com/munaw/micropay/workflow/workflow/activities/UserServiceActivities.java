package com.munaw.micropay.workflow.workflow.activities;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;
import org.springframework.stereotype.Component;

@Activities(version = "1.0")
@ActivityRegistrationOptions(defaultTaskScheduleToStartTimeoutSeconds = 1000,
    defaultTaskStartToCloseTimeoutSeconds = 100)
public interface UserServiceActivities {
}
