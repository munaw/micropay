package com.munaw.micropay.workflow.workflow;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.spring.SpringWorkflowWorker;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;

public class TransferWorkflowWorker extends SpringWorkflowWorker {
    @Inject
    public TransferWorkflowWorker(
        final AmazonSimpleWorkflow service,
        @Value("${aws.swf.domain}") final String domain,
        @Value("${aws.swf.transfer.tasklist}") final String taskListToPoll,
        TransferWorkflow workflow) throws IllegalAccessException, InstantiationException {
        super(service, domain, taskListToPoll);
        addWorkflowImplementation(workflow);
    }
}
