package com.munaw.micropay.workflow;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.munaw.micropay.shared.api.TransactionService;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientExternalFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

import static com.munaw.micropay.workflow.TransactionMetadata.WORKFLOW_ID;

/**
 * @author munaw
 */
@Slf4j
@Component
public class TransactionConfirmationReceiver {

    private static final String QUEUE_NAME = "${sqs.workflow-service-confirmation-topic}";

    @Inject
    private AmazonSimpleWorkflow swfClient;

    @Inject
    private TransactionService transactionService;

    @Inject
    private TransferWorkflowClientExternalFactory transferWorkflowClientExternalFactory;


    @MessageMapping(QUEUE_NAME)
    public void receive(TransferConfirmation confirmation) {
        log.info(confirmation.toString());

        final String workflowId = transactionService.getTransactionMetadata(
            confirmation.getTransactionId(),
            WORKFLOW_ID
        );
        transferWorkflowClientExternalFactory
            .getClient(workflowId)
            .handleConfirmation(confirmation);

    }
}
