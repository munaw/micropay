package com.munaw.micropay.workflow.workflow;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.spring.SpringActivityWorker;
import com.munaw.micropay.workflow.workflow.activities.TransferActivities;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;

public class TransferActivityWorker extends SpringActivityWorker {
    @Inject
    public TransferActivityWorker(
        final AmazonSimpleWorkflow service,
        @Value("${aws.swf.domain}") final String domain,
        @Value("${aws.swf.transfer.tasklist}") final String taskListToPoll,
        TransferActivities activities
    ) throws IllegalAccessException, NoSuchMethodException, InstantiationException {
        super(service, domain, taskListToPoll);
        addActivitiesImplementation(activities);
    }
}
