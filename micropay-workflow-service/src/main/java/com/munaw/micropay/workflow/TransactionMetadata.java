package com.munaw.micropay.workflow;

/**
 * @author munaw
 */
public interface TransactionMetadata {
    String WORKFLOW_ID = "WorkflowId";
}
