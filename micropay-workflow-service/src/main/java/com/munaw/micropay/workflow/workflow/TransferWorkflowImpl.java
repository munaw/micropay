package com.munaw.micropay.workflow.workflow;

import com.amazonaws.services.simpleworkflow.flow.DecisionContextProvider;
import com.amazonaws.services.simpleworkflow.flow.DecisionContextProviderImpl;
import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.annotations.Signal;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.amazonaws.services.simpleworkflow.flow.core.Settable;
import com.amazonaws.services.simpleworkflow.flow.core.TryCatch;
import com.munaw.micropay.account.Account;
import com.munaw.micropay.account.ProcessTransferRequest;
import com.munaw.micropay.shared.dto.TransactionId;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.shared.dto.TransferConfirmationResult;
import com.munaw.micropay.shared.dto.TransferRequest;
import com.munaw.micropay.user.UserIdentity;
import com.munaw.micropay.workflow.workflow.activities.TransferActivitiesClient;
import com.munaw.micropay.workflow.workflow.activities.TransferActivitiesClientImpl;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;


@Scope(value = "workflow", proxyMode = ScopedProxyMode.INTERFACES)
@NoArgsConstructor
@Log4j
public class TransferWorkflowImpl implements TransferWorkflow {

    private final TransferActivitiesClient transferActivitiesClient = new TransferActivitiesClientImpl();
    private final int APPROVE_PERIOD = 60;
    private DecisionContextProvider contextProvider
        = new DecisionContextProviderImpl();
    private Settable<Boolean> approved = new Settable<>();
    private Settable<TransferRequest> transferRequestSettable = new Settable<>();
    private Settable<TransactionId> transactionIdSettable = new Settable<>();
    private Settable<Account> recipientAccountSettable = new Settable<>();
    private Settable<UserIdentity> recipientUserIdentitySettable = new Settable<>();
    private Settable<Boolean> acceptanceSettable = new Settable<>();


    @Override
    public void initiateTransaction(final TransferRequest transferRequest) {
        log.info("initiateTransaction");
        transferRequestSettable.set(transferRequest);
        final Promise<Account> senderAccount = transferActivitiesClient.findAccountByUserEmail(transferRequest.getSender().getEmailAddress());
        new TryCatch() {
            @Override
            protected void doTry() throws Throwable {
                Promise<TransactionId> transactionId = createTransaction(senderAccount, transferRequest);
                storeTransactionId(transactionId);
                transferActivitiesClient.sendConfirmationRequest(transactionId);
//                WorkflowClock clock = contextProvider.getDecisionContext().getWorkflowClock();
//                Promise<Void> timer = clock.createTimer(APPROVE_PERIOD);
//                OrPromise approvedOrTimer = new OrPromise(timer, approved);
                // auto-approving
                handleConfirmationResult(approved);
            }

            @Override
            protected void doCatch(Throwable e) throws Throwable {
                if (e instanceof IllegalArgumentException) {
                    noteIllegalSender(transferRequest);
                } else {
                    notifySenderOfUnsuccessfulInitiation(transferRequest);
                }
            }
        };


    }

    @Asynchronous
    public void storeTransactionId(final Promise<TransactionId> transactionId) {
        log.info("storeTransactionId");
        transactionIdSettable.set(transactionId.get());
    }

    @Asynchronous
    public Promise<TransactionId> createTransaction(Promise<Account> senderAccount, TransferRequest transferRequest) {
        log.info("createTransaction");
        if (senderAccount.get() == null) {
            throw new IllegalArgumentException("Invalid sender account: " + transferRequest.getSender().getEmailAddress());
        } else {
            transferRequest.getSender().setAccount(com.munaw.micropay.shared.dto.Account.of(senderAccount.get()));
            return transferActivitiesClient.createTransaction(transferRequest);
        }
    }

    @Override
    @Signal
    public void handleConfirmation(final TransferConfirmation transferConfirmation) {
        log.info("handleConfirmation SIGNAL");
        Promise<TransferConfirmationResult> confirmationResultPromise = transferActivitiesClient.confirmTransaction(transferConfirmation);
        evaluateConfirmationResult(confirmationResultPromise);
    }

    @Asynchronous
    public void evaluateConfirmationResult(Promise<TransferConfirmationResult> confirmationResultPromise) {
        log.info("evaluateConfirmationResult");
        if (!approved.isReady()) {
            approved.set(confirmationResultPromise.get().isApproved());
        }
    }


    @Asynchronous
    public void handleConfirmationResult(Promise approvedOrTimer) {
        new TryCatch() {
            @Override
            protected void doTry() throws Throwable {
                log.info("handleConfirmationResult");
                boolean isApproved = false;
                if (approved.isReady()) {
                    isApproved = approved.get();
                }
                log.info("handleConfirmationResult: isApproved=" + isApproved);
                if (isApproved) {
                    initDelivery(transferRequestSettable.get(), transactionIdSettable.get());
                } else {
                    cancelTransaction();
                }
            }

            @Override
            protected void doCatch(Throwable e) throws Throwable {
                cancelTransaction();
            }
        };


    }


    @Asynchronous
    public void initDelivery(final TransferRequest transferRequest, final TransactionId id) {
        log.info("initDelivery");
        Promise<Account> recipientAccount = transferActivitiesClient.findAccountByUserEmail(transferRequest.getRecipient().getEmailAddress());
        inviteUserIfNotRegistered(recipientAccount);
        transferAccepted(transactionIdSettable.get(), recipientAccountSettable, acceptanceSettable);

        acceptanceSettable.set(true); //@TODO: make it depending on user acceptance
    }

    @Asynchronous
    public void inviteUserIfNotRegistered(final Promise<Account> accountPromise) {
        log.info("inviteUserIfNotRegistered");
        if (accountPromise.get() == null) {
            throw new RuntimeException("Recepient has no account");
            // @TODO invite for registration
        } else {
            recipientAccountSettable.set(accountPromise.get());
        }
    }

    @Asynchronous
    protected void transferAccepted(final TransactionId id, Settable<Account> accountSettable, final Settable<Boolean> acceptanceSettable) {
        new TryCatch() {
            @Override
            protected void doTry() throws Throwable {
                log.info("transferAccepted");
                TransferRequest transferRequest = transferRequestSettable.get();
                ProcessTransferRequest processTransferRequest = ProcessTransferRequest.builder()
                    .requestId(transferRequest.getRequestId())
                    .sender(transferRequest.getSender().getAccount())
                    .recipient(com.munaw.micropay.shared.dto.Account.of(accountSettable.get()))
                    .build();

                transferActivitiesClient.processTransfer(processTransferRequest);
            }

            @Override
            protected void doCatch(Throwable e) throws Throwable {
                cancelTransaction();
            }
        };
    }

    private void noteIllegalSender(TransferRequest transferRequest) {
        // recording illegal sender call
    }

    @Asynchronous
    private void notifySenderOfUnsuccessfulInitiation(TransferRequest transferRequest) {
        // @TODO
    }

    @Asynchronous
    private void cancelTransaction() {
        // @TODO
    }
}
