package com.munaw.micropay.workflow;

import com.amazonaws.services.simpleworkflow.flow.junit.spring.FlowSpringJUnit4ClassRunner;
import com.munaw.micropay.WorkflowManagerApplication;
import com.munaw.micropay.account.AccountEntity;
import com.munaw.micropay.account.AccountRepository;
import com.munaw.micropay.proxy.mail.MailProxyApplication;
import com.munaw.micropay.proxy.mail.send.EmailDescriptor;
import com.munaw.micropay.proxy.mail.send.EmailSenderService;
import com.munaw.micropay.shared.dto.TransactionId;
import com.munaw.micropay.shared.dto.TransferConfirmation;
import com.munaw.micropay.transactionmanager.Transaction;
import com.munaw.micropay.transactionmanager.TransactionRepository;
import com.munaw.micropay.user.UserIdentityService;
import lombok.extern.log4j.Log4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

@Log4j
@RunWith(FlowSpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
    WorkflowManagerApplication.class,
    MailProxyApplication.class,
    TransferWorkflowIntegrationTest.TransferWorkflowTestConfiguration.class
})
@ActiveProfiles({"workflow-integtest"})
public class TransferWorkflowIntegrationTest {

    @Inject
    TransferRequestReceiver requestReceiver;

    @Inject
    TransactionConfirmationReceiver transactionConfirmationReceiver;

    @Inject
    UserIdentityService userIdentityService;

    @Inject
    AccountRepository accountRepository;

    @Inject
    TransactionRepository transactionRepository;

    @Inject
    EmailSenderService emailSenderService;


    @Test(timeout = 60000)
    public void startLifecycle() throws Exception {
        String requestId = UUID.randomUUID().toString();
        String senderAccountId = createAccount().getId();
        String recepientAccountId = createAccount().getId();
        String senderEmailAddress = "munaw+" + requestId + "@ses.munaw.com";
        String recepientEmailAddress = "manokovacs+" + requestId + "@gmail.com";
        Integer amount = (int) (Math.random() * 900 + 100);
        emailSenderService.send(EmailDescriptor.builder()
            .senderEmail("munaw@munaw.com")
            .recipientEmail("manokovacs@gmail.com")
            .ccEmails(new HashSet<>(Arrays.asList("send@aws.munaw.com")))
            .subject(amount.toString() + " USD")
            .text("Hey, here is some money, Cheers")
            .build());

//        User sender = User.builder()
//            .accountId(senderAccountId)
//            .email(senderEmailAddress)
//            .build();
//        User recipient = User.builder()
//            .accountId(recepientAccountId)
//            .email(recepientEmailAddress)
//            .build();
//        userIdentityService.save(sender);
//        userIdentityService.save(recipient);
//
//        requestReceiver.receive(TransferRequest.builder()
//            .money(Money.of(100, "USD"))
//            .sender(ParticipantDetails.withEmail(senderEmailAddress))
//            .recipient(ParticipantDetails.withEmail(recepientEmailAddress))
//            .requestId(requestId)
//            .build());
        int i = 80;
        Transaction trx = null;
        while (i-- > 0) {
            log.info(i);
            if (trx == null) {
                trx = transactionRepository.findTop1ByOrderByCreatedAtDesc();
                log.info(trx);
                if (trx != null && trx.getValue().getAmount().doubleValue() == amount) {
                    transactionConfirmationReceiver.receive(TransferConfirmation.builder()
                        .confirmationCode(trx.getConfirmationCode())
                        .otp("abc")
                        .transactionId(new TransactionId(trx.getId()))
                        .build()
                    );
                } else {
                    trx = null;
                }
            }
            Thread.sleep(1000);
        }
    }

    private AccountEntity createAccount() {
        AccountEntity accountEntity = new AccountEntity();
        accountRepository.save(accountEntity);
        return accountEntity;
    }

    @Profile("workflow-integtest")
    @Configuration
    @Log4j
    static public class TransferWorkflowTestConfiguration {
    }
}
