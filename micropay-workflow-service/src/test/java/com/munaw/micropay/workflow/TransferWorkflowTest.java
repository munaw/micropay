package com.munaw.micropay.workflow;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.simpleworkflow.flow.core.TryCatchFinally;
import com.amazonaws.services.simpleworkflow.flow.junit.WorkflowTestBase;
import com.amazonaws.services.simpleworkflow.flow.junit.spring.FlowSpringJUnit4ClassRunner;
import com.amazonaws.services.simpleworkflow.flow.junit.spring.SpringWorkflowTest;
import com.amazonaws.services.simpleworkflow.flow.spring.WorkflowScope;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.munaw.micropay.WorkflowManagerApplication;
import com.munaw.micropay.shared.dto.TransferRequest;
import com.munaw.micropay.workflow.workflow.TransferWorkflow;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClient;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientFactory;
import com.munaw.micropay.workflow.workflow.TransferWorkflowClientFactoryImpl;
import com.munaw.micropay.workflow.workflow.activities.TransferActivities;
import com.munaw.micropay.workflow.workflow.activities.UserServiceActivities;
import lombok.extern.log4j.Log4j;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.test.context.ActiveProfiles;

import javax.inject.Inject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Log4j
@RunWith(FlowSpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
    WorkflowManagerApplication.class,
    TransferWorkflowTest.TransferWorkflowTestConfiguration.class
})
@ActiveProfiles({"workflow-untitest"})
public class TransferWorkflowTest {

    @Inject
    @Rule
    public WorkflowTestBase workflowTest;
    @Inject
    private TransferWorkflowClientFactory clientFactory;

    @Test(timeout = 30000)
    @Ignore
    public void startLifecycle() throws Exception {
        final TransferWorkflowClient client = clientFactory.getClient();
        TryCatchFinally tryCatchFinally = new TryCatchFinally() {
            @Override
            protected void doTry() throws Throwable {
                TransferRequest transferRequest = new TransferRequest();
                client.initiateTransaction(transferRequest);

//                workflowTest.clockAdvanceSeconds((startTime + closeAfter * 1000 - now) / 1000 + 100);
            }

            @Override
            protected void doCatch(final Throwable e) throws Throwable {
                e.printStackTrace();
                throw e;
            }

            @Override
            protected void doFinally() throws Throwable {
//                verify(activities, times(1)).askTouristToReview(orderId);
            }
        };

    }

    @Profile("workflow-untitest")
    @Configuration
    @Log4j
    static public class TransferWorkflowTestConfiguration {
        @Bean
        public CustomScopeConfigurer customScopeConfigurer() {
            final CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
            customScopeConfigurer.addScope("workflow", new WorkflowScope());
            return customScopeConfigurer;
        }

        @Bean
        @Primary
        public AWSCredentialsProvider getAWSCredentialsProvider() {
            return Mockito.mock(AWSCredentialsProvider.class);
        }

        @Primary
        @Bean
        public AmazonSQSAsync amazonSQSAsync() {
            AmazonSQSAsync mock = Mockito.mock(AmazonSQSAsync.class);
            when(mock.getQueueUrl(any(GetQueueUrlRequest.class)))
                .thenReturn(Mockito.mock(GetQueueUrlResult.class));
            return mock;
        }

        @Bean
        @Inject
        public SpringWorkflowTest workflowTestBase(TransferActivities transferActivities,
                                                   UserServiceActivities userServiceActivities,
                                                   TransferWorkflow transferWorkflow
        ) throws Exception {
            SpringWorkflowTest workflowTest = new SpringWorkflowTest();
            workflowTest.addWorkflowImplementation(transferWorkflow);
            workflowTest.addActivitiesImplementation(transferActivities);
            workflowTest.addActivitiesImplementation(userServiceActivities);
            return workflowTest;
        }


        @Bean
        @Scope("workflow")
        public TransferWorkflowClientFactory transferWorkflowClientFactory() {
            return new TransferWorkflowClientFactoryImpl();
        }

    }
}
