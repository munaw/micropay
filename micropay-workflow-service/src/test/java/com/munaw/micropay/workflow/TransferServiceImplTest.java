package com.munaw.micropay.workflow;

import com.munaw.micropay.WorkflowManagerApplication;
import com.munaw.micropay.shared.dto.Money;
import com.munaw.micropay.shared.dto.ParticipantDetails;
import com.munaw.micropay.shared.dto.TransferRequest;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * @author munaw
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WorkflowManagerApplication.class)
@IntegrationTest
public class TransferServiceImplTest {
    @Autowired
    TransferRequestReceiver transferRequestReceiver;

    @Test
    @Ignore
    public void testReceiveAndStoreRequest() {
        transferRequestReceiver.receive(
            TransferRequest.builder()
                .requestId("123123")
                .sender(ParticipantDetails.withEmail("munaw@munaw.com"))
                .recipient(ParticipantDetails.withEmail("manokovacs@gmail.com"))
                .money(new Money(new BigDecimal(100.0), Currency.getInstance("USD")))
                .build()
        );
    }

}
